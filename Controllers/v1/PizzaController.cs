using System.Collections.Generic;
using System.Linq;
using api_pizzaria.Dominio.Entidades;
using api_pizzaria.Dominio.Repositorios.Classes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace api_pizzaria.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PizzaController : ControllerBase
    {
        private IConfiguration configuration;
        private RepositorioPizza repositorio;
        public PizzaController(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.repositorio = new RepositorioPizza(this.configuration);
        }
        /// <summary>
        /// Obtem todas as pizzas
        /// </summary>
        /// <returns>lista de pizza</returns>
        
        [HttpGet]
        public ActionResult<IEnumerable<Pizza>> ObterTodas()
        {
            try{
                return new OkObjectResult(this.repositorio.ObterTodas());
            }catch{
                return new NotFoundResult();
            }
        }
        /// <summary>
        /// Obtem todas as pizzas por id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>pizza</returns>
        [HttpGet("{id}")]
        public ActionResult<Pizza> ObterPorID(int id)
        {
            try{
                return new OkObjectResult(this.repositorio.ObterPorID(id));
            }catch{
                return new NotFoundObjectResult($"ID:{id} não encontrado");
            }
        }
        /// <summary>
        /// Criar uma nova pizza
        /// </summary>
        /// <param name="pizza"></param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public ActionResult Criar([FromBody] Pizza pizza)
        {
            if( pizza.Sabor == null || pizza.Tipo == null )
            {
                return new BadRequestObjectResult("Sabor e tipo devem ser informados");
            }
            repositorio.Criar(pizza);
            return new CreatedResult("Criado com sucesso",pizza);
        }
        /// <summary>
        /// Atualiza uma pizza pelo id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pizza"></param>
        /// <returns>ActionResult</returns>
        [HttpPut("{id}")]
        public ActionResult Atualizar(int id, [FromBody] Pizza pizza)
        {
            if(pizza.Sabor == null || pizza.Tipo == null )
            {
                return new BadRequestObjectResult("Sabor e tipo devem ser informados");
            }else if(!repositorio.ObterPorID(id).Any())
            {
                return new NotFoundObjectResult($"ID:{id} não encontrado");
            }
            this.repositorio.Atualizar(id,pizza);
            return new OkObjectResult("Atualizado com sucesso...");
        }
        /// <summary>
        /// Deleta uma pizza pelo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ActionResult</returns>
        [HttpDelete("{id}")]
        public ActionResult Deletar(int id)
        {
             if(!repositorio.ObterPorID(id).Any())
            {
                return new NotFoundObjectResult($"ID:{id} não encontrado");
            }
            repositorio.Deletar(id);
            return new OkObjectResult("Deletado com sucesso...");
        }

    }
}