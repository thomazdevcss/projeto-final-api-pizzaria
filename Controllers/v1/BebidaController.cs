using System.Collections.Generic;
using System.Linq;
using api_pizzaria.Dominio.Entidades;
using api_pizzaria.Dominio.Repositorios.Classes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace api_pizzaria.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BebidaController : ControllerBase
    {
        private IConfiguration configuration;
        private RepositorioBebida repositorio;
        public BebidaController(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.repositorio = new RepositorioBebida(this.configuration);
        }
        /// <summary>
        /// Obtem uma lista de bebidas
        /// </summary>
        /// <returns>lista de bebidas</returns>
        [HttpGet]
        public ActionResult<IEnumerable<Bebida>> ObterTodas()
        {
            try{
                return new OkObjectResult(this.repositorio.ObterTodas());
            }catch{
                return new NotFoundResult();
            }
        }
        /// <summary>
        /// Obtem uma unica bebida
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Bebibda</returns>
        [HttpGet("{id}")]
        public ActionResult<Bebida> ObterPorID(int id)
        {
            try{
                return new OkObjectResult(this.repositorio.ObterPorID(id));
            }catch{
                return new NotFoundResult();
            }
        }
        /// <summary>
        /// Cria uma nova bebida
        /// </summary>
        /// <param name="bebida"></param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public ActionResult Criar([FromBody] Bebida bebida)
        {
            if(bebida.Nome == null || bebida.Valor <= 0)
            {
                return new BadRequestResult();
            }
            repositorio.Criar(bebida);
            return new CreatedResult("Criado com sucesso",bebida);
        }
        /// <summary>
        /// Atualiza uma bebida
        /// </summary>
        /// <param name="id"></param>
        /// <param name="bebida"></param>
        /// <returns>ActionResult</returns>
        [HttpPut("{id}")]
        public ActionResult Atualizar(int id, [FromBody] Bebida bebida)
        {
            if(bebida.Nome == null || bebida.Valor <= 0 || !repositorio.ObterPorID(id).Any())
            {
                return new BadRequestResult();
            }
            this.repositorio.Atualizar(id,bebida);
            return new OkObjectResult("Atualizado com sucesso...");
        }
        /// <summary>
        /// Deleta uma bebida
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ActionResult</returns>
        [HttpDelete("{id}")]
        public ActionResult Deletar(int id)
        {
            if(!repositorio.ObterPorID(id).Any())
            {
                return new BadRequestObjectResult($"ID: {id} não encontrado");
            }
            repositorio.Deletar(id);
            return new OkObjectResult("Deletado com sucesso...");
        }

    }
}