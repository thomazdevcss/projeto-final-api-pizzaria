using System.Collections.Generic;
using api_pizzaria.Dominio.Entidades;
using api_pizzaria.Dominio.Repositorios.Classes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace api_pizzaria.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PedidoController : ControllerBase
    {
        private IConfiguration configuration;
        private RepositorioPedido repositorio;
        public PedidoController(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.repositorio = new RepositorioPedido(this.configuration);
        }
        /// <summary>
        /// Retorna uma lista de pedidos
        /// </summary>
        /// <returns>lista de pedidos</returns>
        
        [HttpGet]
        public ActionResult<IEnumerable<Pedido>> ObterTodos()
        {
            try{
                return new OkObjectResult(this.repositorio.ObterTodos());
            }catch{
                return new NotFoundResult();
            }
        }
        /// <summary>
        /// Retorna uma lista de pedidos em um periodo de um dia
        /// </summary>
        /// <param name="dia"></param>
        /// <returns>lista de pedidos</returns>
        [HttpGet("{dia}")]
        public ActionResult<IEnumerable<Pedido>> ObterTodosPorDia(DateTime dia)
        {
            try{
                return new OkObjectResult(this.repositorio.ObterPorDia(dia));
            }catch{
                return new NotFoundResult();
            }
        }
        /// <summary>
        /// Efetua um pedido
        /// </summary>
        /// <param name="pedido"></param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public ActionResult Efetuar([FromBody] Pedido pedido)
        {
            try{
                repositorio.Efetuar(pedido);
            }catch(Exception e){
                return new BadRequestObjectResult(e.Message);
            }
             return new CreatedResult("Criado com sucesso",pedido);
        }
        /// <summary>
        /// Atualiza um pedido
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pedido"></param>
        /// <returns>ActionResult</returns>
        [HttpPut("{id}")]
        public ActionResult Atualizar(int id, [FromBody] Pedido pedido)
        {
            if(repositorio.Obter(id).Cliente == null)
            {
                return new NotFoundObjectResult($"ID:{id} não encontrado");
            }
            try{
                repositorio.Alterar(id,pedido);
            }catch(Exception e){
                return new BadRequestObjectResult(e.Message);
            }
            return new CreatedResult("Atualizado com sucesso",pedido);
        }
        /// <summary>
        /// Deleta um pedido
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ActionResult</returns>
        [HttpDelete("{id}")]
        public ActionResult Cancelar(int id)
        {
            if(repositorio.Obter(id).Cliente == null)
            {
                return new NotFoundObjectResult($"ID:{id} não encontrado");
            }
            repositorio.Cancelar(id);
            return new OkObjectResult("Deletado com sucesso...");
        }
    }
}