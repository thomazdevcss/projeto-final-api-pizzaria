using System.Collections.Generic;
using System.Linq;
using api_pizzaria.Dominio.Entidades;
using api_pizzaria.Dominio.Repositorios.Classes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
namespace api_pizzaria.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private IConfiguration configuration;
        private RepositorioCliente repositorioCliente;
        private RepositorioPedido repositorioPedido;
    
        public ClienteController(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.repositorioCliente = new RepositorioCliente(this.configuration);
            this.repositorioPedido = new RepositorioPedido(this.configuration);
        }
        /// <summary>
        /// Obtem uma lista de clientes
        /// </summary>
        /// <returns>lista de clientes</returns>
        [HttpGet]
        public ActionResult<IEnumerable<Cliente>> ObterTodos()
        {
            try{
                return new OkObjectResult(this.repositorioCliente.ObterTodos());
            }catch{
                return new NotFoundResult();
            }
        }
        /// <summary>
        /// Obtem os 10 clientes que mais compraram no estabelecimento
        /// </summary>
        /// <returns>lista de clientes</returns>
        [HttpGet("{assiduos}")]
        public ActionResult<Cliente> ObterAssiduos()
        {
            var listaAssiduos = new List<Cliente>();
            var listaPedido = this.repositorioPedido.ObterTodos().ToList();
            
            listaPedido.ForEach(pedido => {
                var numeroPedido = listaPedido.FindAll(pedido_numero => pedido_numero.Cliente == pedido.Cliente).Count();
                var cliente = this.repositorioCliente.ObterTodos().ToList().Find(clientes => clientes.Nome == pedido.Cliente);
                cliente.Numero_Compras = numeroPedido;
                listaAssiduos.Add(cliente);
                if(listaAssiduos.FindAll(t => t.Nome == pedido.Cliente).Count > 1)
                {
                    var index = listaAssiduos.FindIndex(listaIndex => listaIndex.Nome == pedido.Cliente);
                    listaAssiduos.RemoveAt(index);
                }
            });
            
            return new OkObjectResult(listaAssiduos.Take(10).OrderByDescending(x => x.Numero_Compras));
        }
        /// <summary>
        /// Cria um novo cliente
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public ActionResult Criar([FromBody] Cliente cliente)
        {
            repositorioCliente.Criar(cliente);
            return VerificarCampos(cliente,new CreatedResult("Criado com sucesso",cliente));
        }
        /// <summary>
        /// Atualiza um cliente
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cliente"></param>
        /// <returns>ActionResult</returns>
        [HttpPut("{id}")]
        public ActionResult Atualizar(int id, [FromBody] Cliente cliente)
        {
            if(!repositorioCliente.Obter(id).Any())
            {
                return new NotFoundObjectResult($"ID: {id} não encontrado");
            }
            this.repositorioCliente.Atualizar(id,cliente);
            return VerificarCampos(cliente,new OkObjectResult("Atualizado com sucesso..."));
        }
        /// <summary>
        /// Deleta um cliente
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ActionResult</returns>
        [HttpDelete("{id}")]
        public ActionResult Deletar(int id)
        {
            if(!repositorioCliente.Obter(id).Any())
            {
                return new NotFoundObjectResult($"ID: {id} não encontrado");
            }
            repositorioCliente.Deletar(id);
            return new OkObjectResult("Deletado com sucesso...");  
        }
        /// <summary>
        /// Verifica os campos do cliente
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="result"></param>
        /// <returns>ActionResult</returns>
        private ActionResult VerificarCampos(Cliente cliente,ActionResult result)
        {
            if(cliente.Nome == null || cliente.Telefone == null || cliente.Endereco == null)
            {
                return new BadRequestObjectResult("nome,telefone e endereço devem ser informados");
            }
            return result;
        }
       
    }
}