Api Pizzaria
api para controle de uma pizzaria.

Pré-requisitos
Um banco de dados SQL com as seguintes tabelas:
Pizza(colunas: ID(int), Sabor(string),Tipo(string)),
Bebida(colunas: ID(int), Nome(string), Valor(double)),
Cliente(colunas: ID(int), Nome(string), Endereco(string), Telefone(string)),
Pedido(colunas: ID(int),Cliente(string) ,Sabor1(string) ,Sabor2(string) , Sabor3(string), TamanhoPizza(string), Bebida(string), Data,Valor(double),ValorPizza(double),ValorBebida(double),TempoEntrega(string),TaxaEntrega(double)),
.NETCORE Versão 2.2.402 ou superior

Documentação
https://localhost:5001/index.html

Endpoints
/api/v1/pizza
/api/v1/bebida
/api/v1/cliente
/api/v1/pedido

Feito em
• .NETCORE WEB API versão(2.2.402),
• C#

Autor
Thomaz Carvalho Siqueira de Souza
Contato - Thomazdevcss@gmail.com

Versão v1.0