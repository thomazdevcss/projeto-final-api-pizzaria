namespace api_pizzaria.Dominio.Entidades
{
    /// <summary>
    /// Entidade Bebida
    /// </summary>
    public class Bebida
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public double Valor { get; set; }
    }
}