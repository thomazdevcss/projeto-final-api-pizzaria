namespace api_pizzaria.Dominio.Entidades
{
    /// <summary>
    /// Entidade pizza
    /// </summary>
    public class Pizza
    {
        public int ID {get; set;}
        public string Sabor {get; set;}
        public string Tipo {get; set;}
    }
}