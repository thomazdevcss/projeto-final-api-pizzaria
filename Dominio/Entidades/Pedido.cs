using System;

namespace api_pizzaria.Dominio.Entidades
{
    /// <summary>
    /// Entidade Pedido
    /// </summary>
    public class Pedido
    {
        public int ID { get; set; }
        public string Cliente { get; set; }
        public string Sabor1 { get; set; }
        public string Sabor2 { get; set; }
        public string Sabor3 { get; set; }
        public string TamanhoPizza {get; set;}
        public double ValorPizza {get; set;}
        public string Bebida { get; set; }
        public double ValorBebida {get; set;}
        public DateTime Data { get; set; }
        public double TaxaEntrega {get; set;}
        public double Valor { get; set; }
        public string PrevisaoEntrega {get; set;}
    }
}