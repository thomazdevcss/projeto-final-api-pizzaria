namespace api_pizzaria.Dominio.Entidades
{
    /// <summary>
    /// Entidade Cliente
    /// </summary>
    public class Cliente
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get; set; }
        public int Numero_Compras {get; set;}
    }
}