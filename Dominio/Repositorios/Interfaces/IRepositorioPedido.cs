using System;
using System.Collections.Generic;
using api_pizzaria.Dominio.Entidades;

namespace api_pizzaria.Dominio.Repositorios.Interfaces
{
    public interface IRepositorioPedido
    {
         void Efetuar(Pedido pedido);
         void Alterar(int id, Pedido pedido);
         void Cancelar(int id);
         IEnumerable<Pedido> ObterTodos();
         IEnumerable<Pedido> ObterPorDia(DateTime dia);


    }
}