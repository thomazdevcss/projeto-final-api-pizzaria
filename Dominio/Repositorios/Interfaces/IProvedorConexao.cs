using System.Data;

namespace api_pizzaria.Dominio.Repositorios.Interfaces
{
    public interface IProvedorConexao
    {
         IDbConnection Conectar();
    }
}