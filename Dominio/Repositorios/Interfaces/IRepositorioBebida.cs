using System.Collections.Generic;
using api_pizzaria.Dominio.Entidades;
namespace api_pizzaria.Dominio.Repositorios.Interfaces
{
    public interface IRepositorioBebida
    {
         void Criar(Bebida bebida);
         void Atualizar(int id, Bebida bebida);
         void Deletar(int id);
         IEnumerable<Bebida> ObterTodas();
         IEnumerable<Bebida> ObterPorID(int id);
    }
}