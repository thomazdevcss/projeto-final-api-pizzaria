using System.Collections.Generic;
using api_pizzaria.Dominio.Entidades;

namespace api_pizzaria.Dominio.Repositorios.Interfaces
{
    public interface IRepositorioCliente
    {
         void Criar(Cliente cliente);
         void Atualizar(int id, Cliente cliente);
         void Deletar(int id);
         IEnumerable<Cliente> ObterTodos();
    }
}