using System.Collections.Generic;
using api_pizzaria.Dominio.Entidades;

namespace api_pizzaria.Dominio.Repositorios.Interfaces
{
    public interface IRepositorioPizza
    {
         void Criar(Pizza pizza);
         void Atualizar(int id, Pizza pizza);
         void Deletar(int id);
         IEnumerable<Pizza> ObterTodas();
         IEnumerable<Pizza> ObterPorID(int id);
    }
}