using System.Collections.Generic;
using api_pizzaria.Dominio.Entidades;
using api_pizzaria.Dominio.Repositorios.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Linq;
using Dapper;
using System;

namespace api_pizzaria.Dominio.Repositorios.Classes
{
    public class RepositorioCliente : IRepositorioCliente
    {
        private IConfiguration configuration;
        private ProvedorConexao conexao;
        public RepositorioCliente(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.conexao = new ProvedorConexao(this.configuration);
        }
        /// <summary>
        /// Atualiza os dados de um cliente pelo seu ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cliente"></param>
        public void Atualizar(int id, Cliente cliente)
        {
            var param = new DynamicParameters();
            param.Add("@Nome",cliente.Nome);
            param.Add("@Endereco",cliente.Endereco);
            param.Add("@Telefone",cliente.Telefone);
            param.Add("@ID",id);
            var query = @"UPDATE Cliente
                          SET Nome = @Nome,
                              Endereco = @Endereco,
                              Telefone = @Telefone
                              WHERE ID = @ID";
            using(var con = this.conexao.Conectar())
            {
                con.Query(query,param);
            }
        }
        /// <summary>
        /// Cria um novo cliente no banco de dados
        /// </summary>
        /// <param name="cliente"></param>
        public void Criar(Cliente cliente)
        {
            var param = new DynamicParameters();
            param.Add("@Nome",cliente.Nome);
            param.Add("@Endereco",cliente.Endereco);
            param.Add("@Telefone",cliente.Telefone);
            var query = @"INSERT INTO Cliente
                          VALUES(@Nome,@Endereco,@Telefone)";
            using (var con = this.conexao.Conectar())
            {
                con.Query(query,param);
            }
        }
        /// <summary>
        /// Deleta um unico cliente selecionado pelo seu ID
        /// </summary>
        /// <param name="id"></param>
        public void Deletar(int id)
        {
            var query = @"DELETE FROM Cliente
                          WHERE ID = @ID";
            using (var con = this.conexao.Conectar())
            {
                con.Query(query,new{ID = id});
            }
        }
        
        /// <summary>
        /// Obtem uma lista com todos os clientes do estabelecimento
        /// </summary>
        /// <returns>IEnumerable<Cliente></returns>
        public IEnumerable<Cliente> ObterTodos()
        {
            var query = @"SELECT ID,
                                 Nome,
                                 Endereco,
                                 Telefone 
                                 FROM Cliente";
            using (var con = this.conexao.Conectar())
            {
                return con.Query<dynamic>(query).Select(x => new Cliente()
                {
                    ID = x.ID,
                    Nome = x.Nome,
                    Endereco = x.Endereco,
                    Telefone = x.Telefone                   
                });
            }
        }
        public IEnumerable<Cliente> Obter(int id)
        {
            var query = @"SELECT ID,
                                 Nome,
                                 Endereco,
                                 Telefone 
                                 FROM Cliente
                                 WHERE ID = @ID";
            using (var con = this.conexao.Conectar())
            {
                return  con.Query(query,new{ID = id}).Select(x => new Cliente()
                {
                    ID = x.ID,
                    Nome = x.Nome,
                    Endereco = x.Endereco,
                    Telefone = x.Telefone                   
                });   
            }
        }
    }
}