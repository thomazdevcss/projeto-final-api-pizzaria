using System.Data;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using api_pizzaria.Dominio.Repositorios.Interfaces;

namespace api_pizzaria.Dominio.Repositorios.Classes
{
    public class ProvedorConexao : IProvedorConexao
    {
        private IConfiguration configuration;
        private string strCon = "PizzariaConexao";
        public ProvedorConexao(IConfiguration _configuration)
        {   
            this.configuration = _configuration;
        }
        /// <summary>
        /// Fornece a conexão ao banco de dados
        /// </summary>
        /// <returns>IDbConnection</returns>
        public IDbConnection Conectar()
        {
            return new SqlConnection(configuration.GetConnectionString(strCon));
        }
    }
}