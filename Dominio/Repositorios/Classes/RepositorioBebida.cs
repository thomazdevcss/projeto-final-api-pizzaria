using System.Collections.Generic;
using api_pizzaria.Dominio.Entidades;
using api_pizzaria.Dominio.Repositorios.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Linq;
using Dapper;

namespace api_pizzaria.Dominio.Repositorios.Classes
{
    public class RepositorioBebida : IRepositorioBebida
    {
        private IConfiguration configuration;
        private ProvedorConexao conexao;
        public RepositorioBebida(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.conexao = new ProvedorConexao(this.configuration);
        }
        /// <summary>
        /// Atualiza os dados da bebida selecionada pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="bebida"></param>
        public void Atualizar(int id, Bebida bebida)
        {
            var param = new DynamicParameters();
            param.Add("@Nome",bebida.Nome);
            param.Add("@Valor",bebida.Valor);
            param.Add("@ID",id);
            var query = @"UPDATE Bebida
                          SET Nome = @Nome,
                              Valor = @Valor
                              WHERE ID = @ID";
            using(var con = this.conexao.Conectar())
            {
                con.Query(query,param);
            }
        }
        /// <summary>
        /// Criar uma nova bebida
        /// </summary>
        /// <param name="bebida"></param>
        public void Criar(Bebida bebida)
        {
            var param = new DynamicParameters();
            param.Add("@Nome",bebida.Nome);
            param.Add("@Valor",bebida.Valor);
            var query = @"INSERT INTO Bebida
                          VALUES(@Nome,@Valor)";
            using (var con = this.conexao.Conectar())
            {
                con.Query(query,param);
            }
        }
        /// <summary>
        /// Deleta uma bebida selecionada pelo ID
        /// </summary>
        /// <param name="id"></param>
        public void Deletar(int id)
        {
            var query = @"DELETE FROM Bebida
                          WHERE ID = @ID";
            using (var con = this.conexao.Conectar())
            {
                con.Query(query,new{ID = id});
            }
        }
        /// <summary>
        /// Obtem uma unica bebida selecionada pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Bebida</returns>
        public IEnumerable<Bebida> ObterPorID(int id)
        {
            var query = @"SELECT ID,Nome,Valor
                          FROM Bebida
                          WHERE ID = @ID";
            using (var con = this.conexao.Conectar())
            {
                return con.Query(query,new{ID = id}).Select(x => new Bebida()
                {
                    ID = x.ID,
                    Nome = x.Nome,
                    Valor = x.Valor
                });
            }
        }
        /// <summary>
        /// Retorna uma lista de bebidas
        /// </summary>
        /// <returns>IEnumerable<Bebida></returns>
        public IEnumerable<Bebida> ObterTodas()
        {
            var query = @"SELECT ID,Nome,Valor 
                          FROM Bebida";
            using (var con = this.conexao.Conectar())
            {
                return con.Query<dynamic>(query).Select(x => new Bebida()
                {
                    ID = x.ID,
                    Nome = x.Nome,
                    Valor = x.Valor
                });
            }            
        }
    }
}