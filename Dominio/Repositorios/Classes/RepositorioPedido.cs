using System.Collections.Generic;
using api_pizzaria.Dominio.Entidades;
using api_pizzaria.Dominio.Repositorios.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Linq;
using Dapper;
using System;

namespace api_pizzaria.Dominio.Repositorios.Classes
{
    public class RepositorioPedido : IRepositorioPedido
    {
        private IConfiguration configuration;
        private ProvedorConexao conexao;
        public RepositorioPedido(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.conexao = new ProvedorConexao(this.configuration);
        }
        /// <summary>
        /// Alteras os dados de um pedido selecionado pelo seu ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pedido"></param>
        public void Alterar(int id, Pedido pedido)
        {
            var pedidoVerificado = VerificarPedido(pedido);
            var param = new DynamicParameters();
            param.Add("@ID",id);
            param.Add("@Cliente",pedidoVerificado.Cliente);
            param.Add("@Sabor1",pedidoVerificado.Sabor1);
            param.Add("@Sabor2",pedidoVerificado.Sabor2);
            param.Add("@Sabor3",pedidoVerificado.Sabor3);
            param.Add("@TamanhoPizza",pedidoVerificado.TamanhoPizza);
            param.Add("@Bebida",pedidoVerificado.Bebida);
            param.Add("@Data",pedidoVerificado.Data);
            param.Add("@Valor",pedidoVerificado.Valor);
            param.Add("@ValorPizza",pedidoVerificado.ValorPizza);
            param.Add("@ValorBebida",pedidoVerificado.ValorBebida);
            param.Add("@Entrega",pedidoVerificado.PrevisaoEntrega);
            param.Add("@TaxaEntrega",pedidoVerificado.TaxaEntrega);

            var query = @"UPDATE Pedido 
                          SET Cliente = @Cliente,
                              Sabor1 = @Sabor1,
                              Sabor2 = @Sabor2,
                              Sabor3 = @Sabor3,
                              TamanhoPizza = @TamanhoPizza,
                              Bebida = @Bebida,
                              Data = @Data,
                              Valor = @Valor,
                              ValorPizza = @ValorPizza,
                              ValorBebida = @ValorBebida,
                              TempoEntrega = @Entrega,
                              TaxaEntrega = @TaxaEntrega
                              WHERE ID = @ID";

            using(var con = this.conexao.Conectar())
            {
                con.Query(query,param);
            }           
        }
        /// <summary>
        /// Apaga os dados de um pedido do banco de dados selecionado pelo seu ID
        /// </summary>
        /// <param name="id"></param>
        public void Cancelar(int id)
        {
            var query = @"DELETE FROM Pedido
                          WHERE ID = @ID";
            using(var con = this.conexao.Conectar())
            {
                con.Query(query,new{ID = id});
            }
        }
        /// <summary>
        /// Cria um novo pedido no banco de dados
        /// </summary>
        /// <param name="pedido"></param>
        public void Efetuar(Pedido pedido)
        {
            var pedidoVerificado = VerificarPedido(pedido);

            var param = new DynamicParameters();
            param.Add("@Cliente",pedidoVerificado.Cliente);
            param.Add("@Sabor1",pedidoVerificado.Sabor1);
            param.Add("@Sabor2",pedidoVerificado.Sabor2);
            param.Add("@Sabor3",pedidoVerificado.Sabor3);
            param.Add("@TamanhoPizza",pedidoVerificado.TamanhoPizza);
            param.Add("@Bebida",pedidoVerificado.Bebida);
            param.Add("@Data",pedidoVerificado.Data);
            param.Add("@Valor",pedidoVerificado.Valor);
            param.Add("@ValorPizza",pedidoVerificado.ValorPizza);
            param.Add("@ValorBebida",pedidoVerificado.ValorBebida);
            param.Add("@Entrega",pedidoVerificado.PrevisaoEntrega);
            param.Add("@TaxaEntrega",pedidoVerificado.TaxaEntrega);

            var query = @"INSERT INTO Pedido 
                          VALUES (@Cliente,
                                  @Sabor1,
                                  @Sabor2,
                                  @Sabor3,
                                  @TamanhoPizza,
                                  @Bebida,
                                  @Data,
                                  @Valor,
                                  @ValorPizza,
                                  @ValorBebida,
                                  @Entrega,
                                  @TaxaEntrega)";

            using(var con = this.conexao.Conectar())
            {
                con.Query(query,param);
            }           
        }
        /// <summary>
        /// Obtem todos os pedidos por um dia selecionado
        /// </summary>
        /// <param name="dia"></param>
        /// <returns>IEnumerable<Pedido></returns>
        public IEnumerable<Pedido> ObterPorDia(DateTime dia)
        {
            var query = @"SELECT * FROM Pedido 
                          WHERE Data = @DIA";

            var param = new DynamicParameters();
            param.Add("@DIA",dia);

            using(var con = this.conexao.Conectar())
            {
                return con.Query<dynamic>(query,param).Select(x => new Pedido(){
                    ID = x.ID,
                    Cliente = x.Cliente,
                    Sabor1 = x.Sabor1,
                    Sabor2 = x.Sabor2,
                    Sabor3 = x.Sabor3,
                    TamanhoPizza = x.TamanhoPizza,
                    TaxaEntrega = x.TaxaEntrega,
                    Bebida = x.Bebida,
                    Data = x.Data,
                    Valor = x.Valor,
                    ValorPizza = x.ValorPizza,
                    ValorBebida = x.ValorBebida,
                    PrevisaoEntrega = x.TempoEntrega
                });
            }   
        }
        /// <summary>
        /// Obtem todos os pedidos do estabelecimento
        /// </summary>
        /// <returns>IEnumerable<Pedido> </returns>
        public IEnumerable<Pedido> ObterTodos()
        {
            var query = @"SELECT * FROM Pedido";

            using(var con = this.conexao.Conectar())
            {
                return con.Query<dynamic>(query).Select(x => new Pedido(){
                    ID = x.ID,
                    Cliente = x.Cliente,
                    Sabor1 = x.Sabor1,
                    Sabor2 = x.Sabor2,
                    Sabor3 = x.Sabor3,
                    TamanhoPizza = x.TamanhoPizza,
                    TaxaEntrega = x.TaxaEntrega,
                    Bebida = x.Bebida,
                    Data = x.Data,
                    Valor = x.Valor,
                    ValorPizza = x.ValorPizza,
                    ValorBebida = x.ValorBebida,
                    PrevisaoEntrega = x.TempoEntrega
                });
            } 
        
        }
        /// <summary>
        /// Verifica os campos do pedido e os valida
        /// </summary>
        /// <param name="pedido"></param>
        /// <returns>Pedido</returns>
        private Pedido VerificarPedido(Pedido pedido)
        {
            var repBebida = new RepositorioBebida(this.configuration);
            var repPizza = new RepositorioPizza(this.configuration);
            var repCliente = new RepositorioCliente(this.configuration);
            Bebida bebida = new Bebida();
           
            //verifica se existe o cliente
            if(!repCliente.ObterTodos().Any(x => x.Nome == pedido.Cliente))
            {
                throw new Exception("Cliente não encontrado");
            }
            //verifica se os sabores são nulos
            if(pedido.Sabor1 == null)
            {
                throw new Exception("Escolha ao menos um sabor!");
            }
            if(pedido.Sabor2 == null)
            {
               pedido.Sabor2 = null;
            }
            if(pedido.Sabor3 == null)
            {
                pedido.Sabor3 = null;
            }
            //verifica se existem na lista de sabores
            if(pedido.Sabor1 != null && !repPizza.ObterTodas().Any( x => x.Sabor == pedido.Sabor1))
            {
                throw new Exception("Sabor1 não encontrado!");
            }
            if(pedido.Sabor2 != null && !repPizza.ObterTodas().Any( x => x.Sabor == pedido.Sabor2))
            {
                throw new Exception("Sabor2 não encontrado!");
            }
            if(pedido.Sabor3 != null && !repPizza.ObterTodas().Any( x => x.Sabor == pedido.Sabor3))
            {
                throw new Exception("Sabor3 não encontrado!");
            }

            //verifica se foi solicitada a bebida
            if(pedido.Bebida == null || pedido.Bebida == "")
            {
                bebida.Valor = 0;
            }
            else
            {
                //verifica se existe a bebida na lista de bebidas 
                if(repBebida.ObterTodas().Any(x => x.Nome == pedido.Bebida))
                {
                    //encontra bebida na lista
                    bebida = repBebida.ObterTodas().ToList().Find(x => x.Nome == pedido.Bebida);
                    //define o valor da bebida
                    pedido.ValorBebida = bebida.Valor;
                }else
                {
                    throw new Exception("Bebida não encontrada!");
                }
            }

            //determina o preço pelo tamanho da pizza
            if(pedido.TamanhoPizza.ToLower() == "p")
            {
                pedido.ValorPizza = 25.00;
            }
            else if(pedido.TamanhoPizza.ToLower() == "m")
            {
                pedido.ValorPizza = 35.00;
            }
            else if(pedido.TamanhoPizza.ToLower() == "g")
            {
                pedido.ValorPizza = 55.00;
            }
            else
            {
                throw new Exception("Tamanho Invalido, Tamanhos validos são P,M OU G!");
            }

            pedido.PrevisaoEntrega = DateTime.Now.AddMinutes(60).ToString("hh:mm");
            //taxa unica de entrega
            pedido.TaxaEntrega = 5.00;
            
            pedido.Data = DateTime.Now.ToLocalTime();
            
            pedido.ValorBebida = bebida.Valor;
            pedido.Valor = pedido.TaxaEntrega + pedido.ValorBebida + pedido.ValorPizza;
            
            return pedido;
        }

        public Pedido Obter(int id)
        {
            var query = @"SELECT * FROM Pedido WHERE ID = @ID";

            using(var con = this.conexao.Conectar())
            {
                return con.Query(query,new{ID = id}).Select(x => new Pedido(){
                    ID = x.ID,
                    Cliente = x.Cliente,
                    Sabor1 = x.Sabor1,
                    Sabor2 = x.Sabor2,
                    Sabor3 = x.Sabor3,
                    TamanhoPizza = x.TamanhoPizza,
                    TaxaEntrega = x.TaxaEntrega,
                    Bebida = x.Bebida,
                    Data = x.Data,
                    Valor = x.Valor,
                    ValorPizza = x.ValorPizza,
                    ValorBebida = x.ValorBebida,
                    PrevisaoEntrega = x.TempoEntrega
                }).FirstOrDefault();
            } 
        }
    }
}