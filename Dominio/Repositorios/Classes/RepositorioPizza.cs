using System.Collections.Generic;
using api_pizzaria.Dominio.Entidades;
using api_pizzaria.Dominio.Repositorios.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Linq;
using Dapper;

namespace api_pizzaria.Dominio.Repositorios.Classes
{
    public class RepositorioPizza : IRepositorioPizza
    {
        private IConfiguration configuration;
        private ProvedorConexao conexao;
        public RepositorioPizza(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.conexao = new ProvedorConexao(this.configuration);
        }
        /// <summary>
        /// Atualiza os dados de um sabor no banco de dados
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pizza"></param>
        public void Atualizar(int id, Pizza pizza)
        {
            var param = new DynamicParameters();
            param.Add("@Sabor",pizza.Sabor);
            param.Add("@Tipo",pizza.Tipo);
            param.Add("@ID",id);
            var query = @"UPDATE Pizza
                          SET Sabor = @Sabor,
                              Tipo = @Tipo
                              WHERE ID = @ID";
            using(var con = this.conexao.Conectar())
            {
                con.Query(query,param);
            }
        }
        /// <summary>
        /// Cria um novo sabor no bancos de dados
        /// </summary>
        /// <param name="pizza"></param>
        public void Criar(Pizza pizza)
        {
            var param = new DynamicParameters();
            param.Add("@Sabor",pizza.Sabor);
            param.Add("@Tipo",pizza.Tipo);
            var query = @"INSERT INTO Pizza
                          VALUES(@Sabor,@Tipo)";
            using (var con = this.conexao.Conectar())
            {
                con.Query(query,param);
            }
        }
        /// <summary>
        /// Deleta um sabor no banco de dados
        /// </summary>
        /// <param name="id"></param>
        public void Deletar(int id)
        {
            var query = @"DELETE FROM Pizza
                          WHERE ID = @ID";
            using (var con = this.conexao.Conectar())
            {
                con.Query(query,new{ID = id});
            }
        }
        /// <summary>
        /// Obtem um unico sabor do banco de dados
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Pizza</returns>
        public IEnumerable<Pizza> ObterPorID(int id)
        {
            var query = @"SELECT ID,Sabor,Tipo
                          FROM Pizza
                          WHERE ID = @ID";
            using (var con = this.conexao.Conectar())
            {
                return con.Query(query,new{ID = id}).Select(x => new Pizza()
                {
                    ID = x.ID,
                    Sabor = x.Sabor,
                    Tipo = x.Tipo
                });
            }
        }
        /// <summary>
        /// Obtem uma lista de todos os sabores do banco de dados
        /// </summary>
        /// <returns>IEnumerable<Pizza></returns>
        public IEnumerable<Pizza> ObterTodas()
        {
            var query = @"SELECT ID,Sabor,Tipo
                          FROM Pizza";
            using (var con = this.conexao.Conectar())
            {
                return con.Query<dynamic>(query).Select(x => new Pizza()
                {
                    ID = x.ID,
                    Sabor = x.Sabor,
                    Tipo = x.Tipo
                });
            }
        }
    }
}